from django.shortcuts import render, redirect
from . import forms,models

# Create your views here.
# def index(request):
#     return render(request, 'index.html')

def jadwal_ku(request):
    if(request.method == "POST"):
        context = forms.formulir(request.POST)
        if (context.is_valid()):
            context1 = models.Jadwal()
            context1.nama_mata_kuliah = context.cleaned_data["nama_mata_kuliah"]
            context1.nama_dosen = context.cleaned_data["nama_dosen"]
            context1.jumlah_sks = context.cleaned_data["jumlah_sks"]
            context1.deskripsi_mata_kuliah = context.cleaned_data["deskripsi_mata_kuliah"]
            context1.tahun_semester = context.cleaned_data["tahun_semester"]
            context1.ruang_kelas = context.cleaned_data["ruang_kelas"] 
            context1.save()
        return redirect("/jadwal")
    else:
        context = forms.formulir()
        context1 = models.Jadwal.objects.all()
        context_dictio = {
            'formulir' : context,
            'jadwal_ku' : context1
        }
    return render(request, 'index.html', context_dictio)
    

def delete(request, pk):
    if(request.method == "POST"):
        context = forms.formulir(request.POST)
        if(context.is_valid()):
            context1 = models.Jadwal()
            context1.nama_mata_kuliah = context.cleaned_data["nama_mata_kuliah"]
            context1.nama_dosen = context.cleaned_data["nama_dosen"]
            context1.jumlah_sks = context.cleaned_data["jumlah_sks"]
            context1.deskripsi_mata_kuliah = context.cleaned_data["deskripsi_mata_kuliah"]
            context1.tahun_semester = context.cleaned_data["tahun_semester"]
            context1.ruang_kelas = context.cleaned_data["ruang_kelas"] 
            context1.save()
        return redirect("/jadwal")
    else:
        models.Jadwal.objects.filter(pk = pk).delete()
        context = forms.formulir()
        context1 = models.Jadwal.objects.all()
        context_dictio = {
            'formulir' : context,
            'jadwal_ku' : context1
        }
        return render(request, 'index.html', context_dictio)

def detail(request, pk):
    models.Jadwal.objects.get(pk = pk)
    context = forms.formulir()
    context_dictio= {
        "detail" : context,
    }
    return render(request, 'detail.html', context_dictio)
