from django.urls import path

from . import views

app_name = 'jadwal'

urlpatterns = [
    #path('', views.index, name='jadwal'),
    path('', views.jadwal_ku, name = 'jadwal'),
    path('<int:pk>/', views.delete, name = 'hapus'),
    path('detail/<int:pk>', views.detail, name='detail'),

    # dilanjutkan ...
]